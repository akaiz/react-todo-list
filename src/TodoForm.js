import React from 'react'
import TextField from '@material-ui/core/TextField'
import { useInputState } from './useInputState'

const TodoForm = ({ saveTodo }) => {
  const { value, reset, onChange } = useInputState('')
  const submitTodo = event => {
    event.preventDefault()
    saveTodo(value)
    reset()
  }

  return (
    <form onSubmit={submitTodo}>
      <TextField
        variant='outlined'
        placeholder='Add Todo'
        margin='normal'
        fullWidth
        onChange={onChange}
        value={value}
      ></TextField>
    </form>
  )
}

export default TodoForm
