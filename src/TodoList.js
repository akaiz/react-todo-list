import React, { useRef } from 'react'
import {
  List,
  ListItem,
  ListItemSecondaryAction,
  ListItemText,
  Checkbox,
  IconButton
} from '@material-ui/core'
import { Delete as DeleteIcon } from '@material-ui/icons'

const TodoList = ({ todos, deleteTodo }) => {
  const checkboxEl = useRef(null)
  return (
    <List>
      {todos.map((todo, index) => (
        <ListItem key={index.toString()} dense button>
          <Checkbox
            tabIndex={-1}
            disableRipple
            ref={checkboxEl}
            onChange={e => {
              console.log(checkboxEl.current)
            }}
          />
          <ListItemText primary={todo.text} />
          <ListItemSecondaryAction>
            <IconButton
              aria-label='Delete'
              onClick={() => {
                deleteTodo(index)
              }}
            >
              <DeleteIcon />
            </IconButton>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  )
}

export default TodoList
