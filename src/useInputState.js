import { useState } from 'react'

export const useInputState = InitialValue => {
  const [value, setValue] = useState(InitialValue)

  return {
    value,
    onChange: event => {
      setValue(event.target.value)
    },
    reset: () => setValue('')
  }
}
