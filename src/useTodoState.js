import { useState } from 'react'

export const useTodoState = InitialValue => {
  const [todos, setTodos] = useState(InitialValue)

  return {
    todos,
    addTodo: todoText => {
      const trimmedText = todoText.trim()
      if (trimmedText.length > 0) setTodos([...todos, { text: trimmedText, checked: false }])
    },
    deleteTodo: todoIndex => {
      todos.splice(todoIndex, 1)
      setTodos(todos)
    }
  }
}
